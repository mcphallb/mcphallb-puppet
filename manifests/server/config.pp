# Set up the puppet server config
class puppet::server::config inherits puppet::config {
  if $puppet::server::passenger {
    # Anchor the passenger config inside this
    class { 'puppet::server::passenger': } -> Class['puppet::server::config']
  }

  # Open read permissions to private keys to puppet group for foreman,
  # proxy etc.
  file { "${puppet::server::ssl_dir}/private_keys":
    group => $puppet::server::group,
    mode  => '0750',
  }

  file { "${puppet::server::ssl_dir}/private_keys/${::fqdn}.pem":
    group => $puppet::server::group,
    mode  => '0640',
  }

  # appends our server configuration to puppet.conf
  File ["${puppet::server::dir}/puppet.conf"] {
    content => template(
      $puppet::server::agent_template,
      $puppet::server::master_template
    ),
  }

  ## If the ssl dir is not the default dir, it needs to be created before
  # running the generate ca cert or it will fail.
  exec {'puppet_server_config-create_ssl_dir':
    creates => $::puppet::server::ssl_dir,
    command => "/bin/mkdir -p ${::puppet::server::ssl_dir}",
    before  => Exec['puppet_server_config-generate_ca_cert'],
  }

  exec {'puppet_server_config-generate_ca_cert':
    creates => $::puppet::server::ssl_cert,
    command => "${puppet::params::puppetca_path}/${puppet::params::puppetca_bin} --generate ${::fqdn}",
    require => File["${puppet::server::dir}/puppet.conf"],
    notify  => Service[$puppet::server::httpd_service],
  }

  file { "${puppet::server::vardir}/reports":
    ensure => directory,
    owner  => $puppet::server::user,
  }

  if $puppet::server::git_repo {

    # location where our puppet environments are located
    file { $puppet::server::envs_dir:
      ensure => directory,
      owner  => $puppet::server::user,
    }

    # need to chown the $vardir before puppet does it, or else
    # we can't write puppet.git/ on the first run

    file { $puppet::server::vardir:
      ensure => directory,
      owner  => $puppet::server::user,
    }

  }
  else
  {
    file { [$puppet::server::envs_dir, '/usr/share/puppet', $puppet::server::common_modules_path]:
      ensure => directory,
    }

    # make sure your site.pp exists (puppet #15106, foreman #1708)
    file { "${puppet::server::manifest_path}/site.pp":
      ensure  => present,
      replace => false,
      content => "# Empty site.pp required (puppet #15106, foreman #1708)\n",
    }

    # setup empty directories for our environments
    puppet::server::env {$puppet::server::environments: }
  }

  if $puppet::server::external_nodes {
    file { "${puppet::server::dir}/external_nodes":
      ensure  => present,
      content => template($puppet::server::external_nodes),
      owner   => $puppet::server::user,
      mode    => '0774',
    }
  }
}
